// following https://xiph.org/ao/doc/ao_example.c

#include <ao/ao.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	/* -- Initialize -- */

	printf("libao example program\n");

	ao_initialize();

	/* -- Setup for default driver -- */

	int default_driver = ao_default_driver_id();

	ao_sample_format format = {
		.bits = 16,
		.rate = 44100,
		.channels = 1,
		.byte_format = AO_FMT_LITTLE,
	};

	/* -- Open driver -- */

	ao_device* device
			= ao_open_live(default_driver, &format, NULL /* no options */);

	if (device == NULL) {
		fprintf(stderr, "Error opening device.\n");
		return EXIT_FAILURE;
	}

	/* -- Play some stuff -- */

	int seconds = 2,
			buf_size = seconds * format.bits / 8 * format.channels * format.rate;
	char* buffer = calloc(buf_size, sizeof(char));

	float freq = 440.0,
				amp = 10000.0;
	int sample, i;
	for (i = 0; i < format.rate * seconds; i++) {
		sample = (int)roundf(amp * sinf(2.0f * M_PI * freq * (float)i / format.rate));

		buffer[2 * i] = sample & 0xff;
		buffer[2 * i + 1] = (sample >> 8) & 0xff;
	}

	ao_play(device, buffer, buf_size);

	/* -- Close and shutdown -- */

	ao_close(device);

	ao_shutdown();

	return EXIT_SUCCESS;
}
