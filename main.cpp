#include <array>
#include <cmath>
#include <numbers>
#include <print>

int main()
{
	const int sample_freq { 44100 },
			num_seconds { 3 },
			size = sample_freq * num_seconds;
	const double amplitude { 1.0 }, freq { 440.0 };

	std::array<double, size> buffer; // buffer array

	for (int i { 0 }; i < size; ++i) {
		buffer[i] = amplitude * std::sin((2 * std::numbers::pi * freq * i) / sample_freq);
	}

	std::println("{} {}", buffer[0], buffer[1]);
}
