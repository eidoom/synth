BIN=main libao sdl2 portaudio libao_cpp

.PHONY: all clean

all: $(BIN)

main: %: %.cpp
	g++ $< -o $@ -std=c++23 -O2

# sudo dnf install libao-devel
libao: %: %.c
	gcc $< -o $@ -lao -ldl -lm

libao_cpp: %: %.cpp
	g++ $< -o $@ -std=c++23 -lao -ldl -lm

# sudo dnf install SDL2-devel
sdl2: %: %.c
	gcc $< -o $@ $(shell sdl2-config --cflags --libs) -lm

# sudo dnf install portaudio-devel
portaudio: %: %.c
	gcc $< -o $@ -lportaudio -lm

clean:
	-rm $(BIN)
