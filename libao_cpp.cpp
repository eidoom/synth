// following:
// https://xiph.org/ao/doc/ao_example.c
// https://www.acs.psu.edu/drussell/Demos/Pluck-Fourier/Pluck-Fourier.html
// ^(some equations full of mistakes)
// https://en.wikipedia.org/wiki/String_vibration

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <numbers>
#include <print>
#include <vector>

#include <ao/ao.h>

class Synth {
private:
  int modes;
  float d, pi, l, x, a;

  float f(int i) {
    // i = 0 <-> middle C
    // i <-> semi-tome increments
    return std::powf(2.0f, (i - 9.0f) / 12.0f) * 440.0f;
  }

  float mode(float w, int n, float t) {
    return std::sinf(d * n * pi / l) * std::sinf(x * n * pi / l) *
           std::cosf(n * w * t) / (n * n);
  }

public:
  Synth()
      : modes(12),            // number of harmonic modes
        pi(std::numbers::pi), // pi
        l(1),                 // 1m - string length
        d(0.1),               // 10cm - pluck position
        x(0.05)               // 5 cm - pickup position
  {
    float amp{5e5}, // env. amp.
        h{0.01};    // 1cm - pluck displacement
    a = amp * 2.0f * h * l * l / (d * (l - d) * pi * pi);
  }

  void play(std::vector<char> &buffer, int rate, int note, int seconds) {
    // use equation of a plucked string
    // TODO decay

    float freq{f(note)},     // fundamental mode frequency
        w{2.0f * pi * freq}; // ang. freq.

    for (int i{}; i < rate * seconds; ++i) {
      float y{}, t{static_cast<float>(i) / rate};

      for (int n{1}; n <= modes; ++n) {
        y += mode(w, n, t);
      }

      int sample{static_cast<int>(std::roundf(a * y))};

      buffer[2 * i] = sample & 0xff;
      buffer[2 * i + 1] = (sample >> 8) & 0xff;
    }
  }
};

int main(int argc, char **argv) {
  ao_initialize();

  int default_driver{ao_default_driver_id()};

  ao_sample_format format{
      .bits = 16,
      .rate = 44100,
      .channels = 1,
      .byte_format = AO_FMT_LITTLE,
  };

  ao_device *device{
      ao_open_live(default_driver, &format, NULL /* no options */)};

  if (device == NULL) {
    std::println(std::cerr, "Error opening device.");
    return EXIT_FAILURE;
  }

  Synth synth{};

  int seconds{1};
  std::vector<char> buffer(seconds * format.bits / 8 * format.channels *
                           format.rate);

  const std::vector<int> tune{0, 2, 4, 5, 7, 9, 11, 12};

  for (int note : tune) {
    synth.play(buffer, format.rate, note, seconds);
    ao_play(device, buffer.data(), buffer.size());
  }

  ao_close(device);

  ao_shutdown();
}
